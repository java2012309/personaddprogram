package classes;

import java.util.Scanner;

public class Coordinates {
    private float x; //Максимальное значение поля: 271, Поле не может быть null
    private Double y; //Поле не может быть null

    public void setX(Scanner console) {
        try{
            System.out.print("Введите координату X =< 271 : ");
            String strX = console.nextLine();
            x = Float.parseFloat(strX);
            if (x > 271){
                System.out.println("Некорректный ввод. Поле не может быть больше 271. Попробуйте снова.");
                this.setX(console);
            }
        } catch (NullPointerException|NumberFormatException e){
            System.out.println("Некорректный ввод. Получен пустой аргумент. Попробуйте снова.");
            this.setX(console);
        }}
    public void setY(Scanner console) {
        try{
            System.out.print("Введите координату Y : ");
            String strY = console.nextLine();
            this.y = Double.parseDouble(strY);
        } catch (NullPointerException|NumberFormatException e){
            System.out.println("Некорректный ввод. Получен пустой аргумент. Попробуйте снова.");
            this.setY(console);
        }}

    public float getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
