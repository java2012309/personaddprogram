package classes;

import util.AdditionFunctions;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

/**
 * Класс, описывающий элементы объекта Person
 */
public class Person extends Location implements Comparable<Person> {
    private Long id; //Значение поля должно быть больше 0, Значение этого поля должно быть уникальным, Значение этого поля должно генерироваться автоматически
    private String name; //Поле не может быть null, Строка не может быть пустой
    private Coordinates coordinates; //Поле не может быть null
    private Date creationDate; //Поле не может быть null, Значение этого поля должно генерироваться автоматически
    private Long height; //Поле не может быть null, Значение поля должно быть больше 0
    private ZonedDateTime birthday; //Поле не может быть null
    private double weight; //Значение поля должно быть больше 0, Поле не может быть null
    private Color eyeColor; //Поле может быть null
    private Location location; //Поле может быть null

    public Person() {
        id = (long) (Math.random() * 10000 + 1);
    }

    public void setId() { id = (long) (Math.random() * 10000 + 1);
    }

    public void setName(Scanner console) {
        System.out.println("Введите имя: ");
        String strName = console.nextLine();
        if (strName.isEmpty() | strName == null) {
            System.out.println("Некорректный ввод. Получен пустой аргумент. Попробуйте снова.");
            setName(console);
        }
        this.name = strName;
    }

    public void setCoordinates(Scanner console) {
        System.out.println("Введите координаты ");
        Coordinates coordinates = new Coordinates();
        coordinates.setX(console);
        coordinates.setY(console);
        this.coordinates = coordinates;
    }

    public void setCreationDate() {
        this.creationDate = new Date();
    }

    public void setHeight(Scanner console) {
        while (true) {
            try {
                System.out.println("Введите рост: ");
                String strHeight = console.nextLine();
                long longHeight = Long.parseLong(strHeight);

                if (longHeight <= 0) {
                    System.out.println("Некорректный ввод. Значение поля не может быть меньше или равно 0. Попробуйте снова.");
                } else {
                    this.height = longHeight;
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Некорректный ввод. В аргументе содержатся нечисловые символы. Попробуйте снова.");
            }
        }
    }

    public void setBirthday(Scanner console) {
        while (true) {
            try {
                System.out.print("Дата рождения (гггг-мм-дд): ");
                String birthdayString = console.nextLine();

                ZonedDateTime birthday;
                try {
                    birthday = ZonedDateTime.parse(birthdayString + "T00:00:00+00:00");
                    this.birthday = birthday;
                    break; // Выход из цикла после успешной установки
                } catch (Exception e) {
                    System.out.println("Ошибка! Некорректный формат даты.");
                }
            } catch (Exception e) {
                System.out.println("Ошибка: Некорректный формат даты. Попробуйте снова.");
            }
        }
    }


    public void setWeight(Scanner console) {
        while (true) {
            try {
                System.out.println("Введите вес: ");
                String strWeight = console.nextLine();
                double doubleWeight = Double.parseDouble(strWeight);

                if (doubleWeight <= 0) {
                    System.out.println("Некорректный ввод. Значение поля не может быть меньше или равно 0. Попробуйте снова.");
                } else {
                    this.weight = doubleWeight;
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Некорректный ввод. В аргументе содержатся нечисловые символы. Попробуйте снова.");
            }
        }
    }


    public void setEyeColor(Scanner console) {
        System.out.println("Введите цвет глаз: ");
        System.out.println("Значение поля может быть: \n" + Color.nameList());
        String strType = console.nextLine();
        if (strType.isEmpty() | strType == null) {
            System.out.println("Некорректный ввод. Получен пустой аргумент. Попробуйте снова.");
            setEyeColor(console);
        }
        try {
            this.eyeColor = AdditionFunctions.compareeyeColor(strType.toLowerCase(Locale.ROOT));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            setEyeColor(console);
        }
    }

    public void setLocation(Scanner console) {
        System.out.println("Введите данные для местоположения:");
        Location location = new Location();
        location.setX(console);
        location.setY(console);
        location.setZ(console);
        location.setName(console);
        this.location = location;
    }

    public void addParameters(Scanner console) {
        setId();
        setName(console);
        setCoordinates(console);
        setCreationDate();
        setHeight(console);
        setBirthday(console);
        setWeight(console);
        setEyeColor(console);
        setLocation(console);
    }

    public boolean checkParameters() {
        if (getName() == null ||
                getId() == null ||
                getName().isEmpty() ||
                getCoordinates() == null ||
                getCreationDate() == null ||
                getWeight() <= 0 ||
                getBirthday() == null ||
                getHeight() <= 0 ||
                getLocation() == null) {
            return false;
        }

        if (getCoordinates().getX() > 271 || getCoordinates().getY() == null) {
            return false;
        }

        if (getLocation().getY() == null || getLocation().getName() == null) {
            return false;
        }

        if (getType() == null){
            return false;
        }

        return true;
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Long getHeight() {
        return height;
    }

    public ZonedDateTime getBirthday() {
        return birthday;
    }

    public double getWeight() {
        return weight;
    }

    public Color getType() {
        return eyeColor;
    }

    public Location getLocation() {
        return location;
    }


    @Override
    public String toString() {

            return "Person{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", coordinates=" + coordinates.toString() +
                    ", creationDate=" + creationDate +
                    ", height=" + height +
                    ", birthday=" + birthday +
                    ", weight=" + weight +
                    ", eyeColor=" + eyeColor +
                    ", location=" + location.toString() + "}";

    }

    @Override
    public int compareTo(Person o) {
        return 0;
    }

}
