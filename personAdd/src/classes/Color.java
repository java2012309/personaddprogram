package classes;

public enum Color {
    GREEN,
    BLUE,
    YELLOW,
    WHITE,
    BROWN;

    /**
     * Generates a beautiful list of enum string values.
     *
     * @return String with all enum values splitted by comma.
     */
    public static String nameList() {
        String nameList = "";
        for (Color Color : values()) {
            nameList += Color.name() + ", ";
        }
        return nameList.substring(0, nameList.length() - 2);
    }
}