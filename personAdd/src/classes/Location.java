package classes;

import java.util.Scanner;

public class Location {
    private long x;
    private Double y; //Поле не может быть null
    private float z;
    private String name; //Поле не может быть null

    public void setX(Scanner console) {
        try {
            System.out.print("Введите координату локации X: ");
            String strX = console.nextLine();
            this.x = Long.parseLong(strX);
        } catch (NumberFormatException e) {
            System.out.println("Некорректный ввод. Попробуйте снова.");
            this.setX(console);
        }
    }

    public void setY(Scanner console) {
        try {
            System.out.print("Введите координату локации Y: ");
            String strY = console.nextLine();
            this.y = Double.parseDouble(strY);
        } catch (NumberFormatException e) {
            System.out.println("Некорректный ввод. Попробуйте снова.");
            this.setY(console);
        }
    }


    public void setZ(Scanner console) {
        try {
            System.out.print("Введите координату локации Z: ");
            String strZ = console.nextLine();
            this.z = Float.parseFloat(strZ);
        } catch (NumberFormatException e) {
            System.out.println("Некорректный ввод. Попробуйте снова.");
            this.setZ(console);
        }
    }

    public void setName(Scanner console) {
        try {
            System.out.print("Введите имя: ");
            String newName = console.nextLine();
            if (!newName.isEmpty()) {
                this.name = newName;
            } else {
                System.out.println("Ошибка: Имя не может быть пустым. Попробуйте снова.");
                this.setName(console);
            }
        } catch (NullPointerException e) {
            System.out.println("Некорректный ввод. Попробуйте снова.");
            this.setName(console);
        }
    }


    @Override
    public String toString() {
        return "Location{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", name='" + name + '\'' +
                '}';
    }

    public Double getY() {
        return y ;
    }

    public String getName() {
        return name;
    }



    public long getX() {
        return x;
    }

    public float getZ() {
        return z;
    }
}

