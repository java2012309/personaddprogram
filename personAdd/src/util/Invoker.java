package util;

import command.NoArgumentCommand.*;
import command.OneArgumentCommand.*;
import command.ScannerArgumentCommand.*;

import java.util.Scanner;

/**
 * класс инициатора шаблона - это команда, которая запускает методы класса collection
 */
public class Invoker {
    public Invoker(String filename) {
        this.fileName = filename;
    }

    String fileName;
    Command Help;
    Command Info;
    Command Show;
    CommandWithScanner Add;
    CommandWithScanner Update;
    Command Remove_By_Id;
    Command Clear;
    Command Save;
    Command Exit;
    Command Execute_Script;
    Command Remove_At;
    Command Sort;
    Command Reorder;
    Command Print_Field_Descending_Height;
    Command Filter_Greater_Than_Location;

    public void setHelp(Command help) {Help = help;}
    public void setInfo(Command info) {Info = info;}
    public void setShow(Command show) {Show = show;}
    public void setAdd(CommandWithScanner add) {Add = add;}
    public void setUpdate(CommandWithScanner update) {Update = update;}
    public void setRemove_By_Id(Command remove_By_Id) {Remove_By_Id = remove_By_Id;}
    public void setClear(Command clear) {Clear = clear;}
    public void setSave(Command save) {Save = save;}
    public void setExit(Command exit) {Exit = exit;}
    public void setExecute_Script(Command execute_Script) {Execute_Script = execute_Script;}
    public void setRemove_at(OneArgumentCommand remove_at) {Remove_At = remove_at;}
    public void setSort(NoArgumentCommand sort) {Sort = sort;}
    public void setReorder(NoArgumentCommand reorder) {Reorder = reorder;}
    public void setPrint_Field_Descending_Height(NoArgumentCommand print_Field_Descending_Height) {Print_Field_Descending_Height = print_Field_Descending_Height;}
    public void setFilter_Greater_Than_Location(OneArgumentCommand filter_Greater_Than_Location) {Filter_Greater_Than_Location = filter_Greater_Than_Location;}

    public void help() {
        Help.execute();
    }
    public void info() {
        Info.execute();
    }
    public void show() {
        Show.execute();
    }
    public void add(Scanner in) {Add.execute(in);}
    public void update(int id, Scanner in) {
        Update.execute(id, in);
    }
    public void remove_by_id(int id) {Remove_By_Id.execute(id);}
    public void clear() {
        Clear.execute();
    }
    public void save() {Save.execute(fileName);}
    public void exit() { Exit.execute();}

    public void execute_script(String fileName) {Execute_Script.execute(fileName);}
    public void remove_at(int index) {Remove_At.execute(index);}
    public void sort() {Sort.execute();}
    public void reorder() {Reorder.execute();}
    public void print_field_descending_height() {Print_Field_Descending_Height.execute();}
    public void filter_greater_than_location(int value) {Filter_Greater_Than_Location.execute(value);}

    public void setCommands(Collection collection, Scanner scanner) {
        setHelp(new HelpCommand(collection));
        setInfo(new InfoCommand(collection));
        setShow(new ShowCommand(collection));
        setAdd(new AddCommand(collection, scanner));
        setUpdate(new UpdateCommand(collection, scanner));
        setRemove_By_Id(new Remove_By_IdCommand(collection));
        setClear(new ClearCommand(collection));
        setSave(new SaveCommand(collection));
        setExit(new ExitCommand(collection));
        setExecute_Script(new Execute_ScriptCommand(collection));
        setRemove_at(new Remove_AtCommand(collection));
        setSort(new SortCommand(collection));
        setReorder(new ReorderCommand(collection));
        setPrint_Field_Descending_Height(new Print_Field_Descending_HeightCommand(collection));
        setFilter_Greater_Than_Location(new Filter_Greater_Than_LocationCommand(collection));
    }



}
