package util;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * класс для проверки консольных аргументов и шаблонов команд
 */
public class CommandValidator {
    private Invoker invoker;
    public CommandValidator(Invoker invoker) {
        this.invoker = invoker;
    }
    public boolean validate(String commandName, Pattern pattern) {
        Matcher matcher = pattern.matcher(commandName);
        return matcher.matches();
    }

    /**
     * метод сопоставления шаблонов и аргументов, запускает команду через класс invoker
     * @param message
     * @param in
     */
    public void use(String message, Scanner in) {
        String[] tokens = message.split(" ");
        String commandName = tokens[0];
        Pattern HELP = Pattern.compile("\\s*help\\s*");
        Pattern INFO = Pattern.compile("\\s*info\\s*");
        Pattern SHOW = Pattern.compile("\\s*show\\s*");
        Pattern ADD = Pattern.compile("\\s*add\\s*");
        Pattern UPDATE = Pattern.compile("\\s*update\\s*");
        Pattern REMOVE_BY_ID = Pattern.compile("\\s*remove_by_id\\s*");
        Pattern CLEAR = Pattern.compile("\\s*clear\\s*");
        Pattern SAVE = Pattern.compile("\\s*save\\s*");
        Pattern EXIT = Pattern.compile("\\s*exit\\s*");
        Pattern EXECUTE_SCRIPT = Pattern.compile("\\s*execute_script\\s*");
        Pattern REMOVE_AT = Pattern.compile("\\s*remove_at\\s*");
        Pattern SORT = Pattern.compile("\\s*sort\\s*");
        Pattern REORDER  = Pattern.compile("\\s*reorder\\s*");
        Pattern PRINT_FIELD_DESCENDING_HEIGHT = Pattern.compile("\\s*print_field_descending_height\\s*");
        Pattern FILTER_GREATER_THAN_LOCATION = Pattern.compile("\\s*filter_greater_than_location\\s*");


        if (validate(commandName, HELP)) {
            invoker.help();
        } else if (validate(commandName, INFO)) {
            invoker.info();
        } else if (validate(commandName, SHOW)) {
            invoker.show();
        } else if (validate(commandName, SORT)) {
            invoker.sort();
        }else if (validate(commandName, PRINT_FIELD_DESCENDING_HEIGHT)) {
            invoker.print_field_descending_height();
        } else if (validate(commandName, REORDER)) {
            invoker.reorder();
        } else if (validate(commandName, ADD)) {
            invoker.add(in);
        } else if (validate(commandName, UPDATE)) {
            try {
                invoker.update(Integer.parseInt(tokens[1]), in);
            } catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
                System.out.println("Ошибка: Аргумент не интерпретируется в id чел.");
            }
        } else if (validate(commandName, REMOVE_BY_ID)) {
            try {
                invoker.remove_by_id(Integer.parseInt(tokens[1]));
            } catch (NumberFormatException|ArrayIndexOutOfBoundsException e) {
                System.out.println("Ошибка: Аргумент не интерпретируется в id человека.");
            }
        } else if (validate(commandName, CLEAR)) {
            invoker.clear();
        } else if (validate(commandName, REMOVE_AT)) {
            try {
                int index = Integer.parseInt(tokens[1]);
                invoker.remove_at(index);
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                System.out.println("Ошибка: Аргумент не интерпретируется в качестве индекса.");
            }
        } else if (validate(commandName, SAVE)){
            invoker.save();
        }  else if (validate(commandName, EXIT)) {
            invoker.exit();
        }  else if (validate(commandName, EXECUTE_SCRIPT)) {
            try {
                invoker.execute_script(tokens[1]);
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Ошибка: Не указан аргумент.");
            }
        }  else if (validate(commandName, FILTER_GREATER_THAN_LOCATION)) {
            try {
                invoker.filter_greater_than_location(Integer.parseInt(tokens[1]));
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                System.out.println("Ошибка: Аргумент не интерпретируется в значение координаты.");
            }
        }  else {
            System.out.println("Команда не найдена. Введите 'help', чтобы получить информацию о доступных командах.");
        }
    }
}
