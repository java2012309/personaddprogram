package util;

import classes.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * класс для дополнительных методов
 */
public class AdditionFunctions {
    public static boolean compare(String arg, Pattern pattern) {
        Matcher matcher = pattern.matcher(arg);
        return matcher.matches();
    }
    public static Color compareeyeColor(String eyeColor) throws Exception {
        Pattern GREEN = Pattern.compile("\\s*green\\s*");
        Pattern BLUE = Pattern.compile("\\s*blue\\s*");
        Pattern YELLOW = Pattern.compile("\\s*yellow\\s*");
        Pattern WHITE = Pattern.compile("\\s*white\\s*");
        Pattern BROWN = Pattern.compile("\\s*brown\\s*");

        if (compare(eyeColor, GREEN)) {
            return Color.GREEN;
        } else if (compare(eyeColor, BLUE)) {
            return Color.BLUE;
        } else if (compare(eyeColor, YELLOW)) {
            return Color.YELLOW;
        }else if(compare(eyeColor, WHITE)) {
            return Color.WHITE;
        }else if(compare(eyeColor, BROWN)) {
            return Color.BROWN;
        } else if (eyeColor.equals("")) {
            return null;
        } else {
            throw new Exception("Некорректный ввод. Полученный аргумент не интерпретируется в поле.");
        }
    }
}

