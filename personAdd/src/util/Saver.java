package util;

import classes.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * класс, используемый для сериализации и десериализации в json
 */
public class Saver {

    /**
     * сериализация в json
     * @param Person
     * @param filename
     */
    public void serializePerson(Person Person, String filename) {
        try {
            FileOutputStream fos = new FileOutputStream(filename, true);
            XStream xStream = new XStream(new JettisonMappedXmlDriver());
            xStream.alias("Person", Person.class);
            xStream.alias("Coordinates", Coordinates.class);
            fos.write(xStream.toXML(Person).getBytes(UTF_8));
            fos.write("\n".getBytes(UTF_8));
            fos.flush();
            fos.close();
            System.out.println("Человек с id: " + Person.getId() + " сохранен");
        } catch (IOException e) {
            System.out.println("Сериализация не выполнена:" + Person.toString());
        }
    }

    /**
     * десериализация json
     * @param filename
     * @param collection
     */
    public void deserializePerson(String filename, LinkedList<Person> collection) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename), StandardCharsets.UTF_8));
            LinkedList<String> dataFromFile = new LinkedList<>();
            String line = br.readLine();
            int counter = 0;
            while (line != null) {
                dataFromFile.add(line + "\n");
                counter += 1;
                line = br.readLine();
            }
            br.close();
            XStream xStream = new XStream(new JettisonMappedXmlDriver());
            xStream.alias("Person", Person.class);
            xStream.alias("Coordinates", Coordinates.class);
            Class<?>[] classes = new Class[] { Person.class, Coordinates.class, Address.class};
            XStream.setupDefaultSecurity(xStream);
            xStream.allowTypes(classes);
            for (int i = 0; i < counter; ++i) {
                Person Person = (Person)xStream.fromXML(dataFromFile.get(i));
                try {
                    if (Person.checkParameters()) {
                        collection.add(Person);
                        System.out.println("Добавлен: " + Person.toString());
                    }
                    else {
                        System.out.println("Ошибка 1: " + Person.toString());
                    }
                } catch (NullPointerException e){
                    System.out.println("Ошибка 2: " + Person.toString());}
            }
            System.out.println("Файл успешно прочитан.");
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден.");
            System.exit(1);
        } catch (IOException e) {
            System.out.println("IOException");
        }
    }
}
