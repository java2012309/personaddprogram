package util;

import classes.Person;
import exceptions.RecursiveScriptExecuteException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


/**
 * класс, содержащий коллекцию и все методы для работы с ней
 */
public class Collection {
    public Collection(LinkedList<Person> collection) {
        this.collection = collection;
        sortCollection();
    }
    private LinkedList<Person> collection;
    private long[] idArray = new long[100];
    private final java.util.Date creationTime = new Date();
    private final List<String> scriptStack = new ArrayList<>();

    Saver saver = new Saver();

    public void help() {
        System.out.println("Доступные команды:\n" +
                "help: вывести справку по доступным командам\n" +
                "info: вывести в стандартный поток вывода информацию о коллекции (тип, дата инициализации, количество элементов и т.д.)\n" +
                "show: вывести в стандартный поток вывода все элементы коллекции в строковом представлении\n" +
                "add {element} : добавить новый элемент в коллекцию\n" +
                "update id {element} : обновить значение элемента коллекции, id которого равен заданному\n" +
                "remove_by_id id : удалить элемент из коллекции по его id\n" +
                "clear: очистить коллекцию\n" +
                "save: сохранить коллекцию в файл\n" +
                "execute_script file_name: считать и исполнить скрипт из указанного файла. В скрипте содержатся команды в таком же виде, в котором их вводит пользователь в интерактивном режиме\n" +
                "exit: завершить программу (без сохранения в файл)\n" +
                "remove_at index : удалить элемент, находящийся в заданной позиции коллекции (index)\n" +
                "reorder : отсортировать коллекцию в порядке, обратном нынешнему\n" +
                "sort : отсортировать коллекцию в естественном порядке\n" +
                "filter_greater_than_location location : вывести элементы, значение поля location которых больше заданного\n" +
                "print_field_descending_height : вывести значения поля height всех элементов в порядке убывания");

    }
    public void info() {
        System.out.println("Тип коллекции: " + this.collection.getClass());
        System.out.println("Размер коллекции: " + this.collection.size());
        System.out.println("Дата инициализации: " + this.creationTime + "\n");
    }

    public void show() {
        if (collection.size() != 0) {
            for (Person person : collection) {
                System.out.println(person.toString());
            }
        } else {
            System.out.println("Коллекция пуста.");
        }
    }

    public void add(Scanner sc) {
        Person person = new Person();
        person.addParameters(sc);
        checkId(person);
        collection.add(person);
        System.out.println("Элемент успешно добавлен в коллекцию. ");
        sortCollection();
    }

    public void update(int id, Scanner in) {
        Iterator<Person> iterator = collection.iterator();
        boolean found = false;
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if (person.getId() == id) {
                found = true;
                person.addParameters(in);
                System.out.println("Элемент успешно обновлен.");
            }
        }
        if (!found) {
            System.out.println("Элемент с данным id не найден.");
        }
        sortCollection();
    }


    public void remove_by_id(int id) {
        Iterator<Person> iterator = collection.iterator();
        boolean found = false;
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if (person.getId() == id) {
                iterator.remove(); // Используйте iterator.remove() для безопасного удаления
                found = true;
                System.out.println("Элемент успешно удалён.");
            }
        }
        if (!found) {
            System.out.println("Элемент с данным id не найден.");
        }
        sortCollection(); // Вы можете вызвать sortCollection после завершения итерации
    }


    public void clear() {
        collection.clear();
        System.out.println("Коллекция успешно очищена. ");
        sortCollection();
    }

    public void exit() {
        System.out.println("Программа завершена. ");
        System.exit(0);
    }





    public void save(String filename) throws IOException {
        FileWriter fileWriter = new FileWriter(filename);
        fileWriter.write("");
        fileWriter.flush();
        fileWriter.close();
        if (collection.size() != 0) {
            for (Person Person: collection) {
                saver.serializePerson(Person, filename);
            }
        }
        else {
            System.out.println("Коллекция пуста.");
        }
        System.out.println("Коллекция сохранена.");
    }

    public void execute_script(String filename) {
        try {
            Scanner file = new Scanner(new FileReader(filename));
            Invoker user = new Invoker(filename);
            user.setCommands(this, file);
            CommandValidator validator = new CommandValidator(user);
            String line;
            if (scriptStack.contains(filename)) throw new RecursiveScriptExecuteException();
            scriptStack.add(filename);
            while (file.hasNextLine()) {
                System.out.println("Запущен скрипт: " + filename + "\n");
                line = file.nextLine();
                System.out.println("Прочитана команда: " + line);
                validator.use(line, file);
            }
            scriptStack.remove(scriptStack.size() - 1);
        } catch (FileNotFoundException e) {
            System.out.println("Файл со скриптом не найден.");
        } catch (NoSuchElementException e) {
            System.out.println("Файл со скриптом пустой.");
        } catch (RecursiveScriptExecuteException e) {
            System.out.println("Произошла бесконечная рекурсия. Скрипт принудительно завершён.");
        }
    }





    /**
     * метод для сортировки коллекции
     */
    public void sortCollection() {

        ArrayList<Person> ArrayCollection = new ArrayList<>(collection);
        Collections.sort(ArrayCollection);
        collection.clear();
        int i = 0;
        for (Person Person: ArrayCollection) {
            collection.add(Person);
            idArray[i] = Person.getId();
            i++;
        }
    }

    public void checkId(Person Person) {
        for (int i = 0; i<idArray.length; i++) {
            if (Person.getId()==idArray[i]) {
                System.out.println("id человека уже есть в коллекции. Присвоен новый id");
                Person.setId();
                checkId(Person);
            }
        }
    }

    public void sort() {
        if (collection.isEmpty()) {
            System.out.println("Коллекция пуста, невозможно выполнить сортировку.");
            return;
        }

        Collections.sort(collection);
        System.out.println("Коллекция отсортирована.");
        show();
    }

    public void reorder() {
        if (collection.isEmpty()) {
            System.out.println("Коллекция пуста, невозможно изменить порядок элементов.");
            return;
        }

        Collections.reverse(collection);
        System.out.println("Порядок элементов в коллекции изменен.");
        show();
    }

    public void print_field_descending_height() {
        if (collection.size() != 0) {
            List<Person> sortedList = new ArrayList<>(collection);
            sortedList.sort(new Comparator<Person>() {
                @Override
                public int compare(Person person1, Person person2) {
                    return Double.compare(person2.getHeight(), person1.getHeight());
                }
            });

            for (Person person : sortedList) {
                System.out.println(person.getHeight());
            }
        } else {
            System.out.println("Коллекция пуста.");
        }
    }






    public void filter_greater_than_location(int value) {
        System.out.println("Местоположения с хотя бы одной координатой (x, y или z) больше " + value + ":");

        for (Person person : collection) {
            long x = person.getLocation().getX();
            Double y = person.getLocation().getY();
            float z = person.getLocation().getZ();

            if (x > value || y > value || z > value) {
                StringBuilder result = new StringBuilder("Location{");
                if (x > value) {
                    result.append("x=").append(x).append(", ");
                }
                if (y > value) {
                    result.append("y=").append(y).append(", ");
                }
                if (z > value) {
                    result.append("z=").append(z).append(", ");
                }
                result.append("name='").append(person.getLocation().getName()).append("'}");
                System.out.println(result);
            }
        }
    }



    public void remove_at(int index) {
        if (index >= 0 && index < collection.size()) {
            collection.remove(index);
            System.out.println("Элемент с индексом " + index + " успешно удален.");
        } else {
            System.out.println("Ошибка: Недопустимый индекс.");
        }
    }




}
