package command.OneArgumentCommand;

import util.Collection;

public class Filter_Greater_Than_LocationCommand extends OneArgumentCommand{
    public Filter_Greater_Than_LocationCommand(Collection collection) {
        super(collection);
    }

    @Override
    public void execute(int value) {
        super.collection.filter_greater_than_location(value);
    }
}

