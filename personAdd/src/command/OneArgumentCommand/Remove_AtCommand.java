package command.OneArgumentCommand;

import util.Collection;

public class Remove_AtCommand extends OneArgumentCommand{
    public Remove_AtCommand(Collection collection) {
        super(collection);
    }
    @Override
    public void execute(int index) {
        super.collection.remove_at(index);
    }
}
