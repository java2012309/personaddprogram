package command.NoArgumentCommand;

import util.Collection;

public class ReorderCommand extends NoArgumentCommand {
    public ReorderCommand(Collection collection) {
        super(collection);
    }
    @Override
    public void execute() {
        collection.reorder();
    }
}
