package command.NoArgumentCommand;

import util.Collection;

public class Print_Field_Descending_HeightCommand extends NoArgumentCommand {
    public Print_Field_Descending_HeightCommand(Collection collection) {
        super(collection);
    }
    @Override
    public void execute() {
        collection.print_field_descending_height();
    }
}
