package command.NoArgumentCommand;

import util.Collection;

public class SortCommand extends NoArgumentCommand {
    public SortCommand(Collection collection) {
        super(collection);
    }
    @Override
    public void execute() {
        collection.sort();
    }
}
