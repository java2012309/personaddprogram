import classes.Person;
import util.Collection;
import util.CommandValidator;
import util.Invoker;
import util.Saver;


import java.util.LinkedList;
import java.util.Scanner;


public class Main {


    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Не указано имя файла через аргумент командной строки.");
            return;
        }

        String filename = args[0];

        try {
            Saver saver = new Saver();
            LinkedList<Person> person = new LinkedList<>();
            saver.deserializePerson(filename, person);

            Scanner console = new Scanner(System.in);
            Collection collection = new Collection(person);
            collection.sortCollection();

            Invoker user = new Invoker(filename);
            CommandValidator validator = new CommandValidator(user);
            user.setCommands(collection, console);

            System.out.println("Добро пожаловать! Введите в консоль команду 'help', чтобы получить информацию о существующих командах.");

            while (true) {
                System.out.println("Введите команду:");
                String message = console.nextLine();
                validator.use(message, console);
            }
        } catch (Exception e) {
            System.out.println("Произошла ошибка: " + e.getMessage());
        }
    }

}
